# OpenApiParser
Converts OpenAPI Specification to code ( `CSharpCodeWriter`)

## Usage
```bash
dotnet run -- -u https://petstore.swagger.io/v2/swagger.json -o Sample
```

```bash
dotnet OpenApiParser.dll -u https://petstore.swagger.io/v2/swagger.json -o Sample
```
